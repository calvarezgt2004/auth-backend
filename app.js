const express = require('express');
const cors = require('cors');
const app = express();

// IMPORTACION RUTAS

const userRoutes = require('./src/routes/user.routes')

// MIDDLEWARES
app.use(express.urlencoded({ extended: false}));
app.use(express.json());

// CABECERAS
app.use(cors());

// CARGA DE RUTAS

app.use('/api', userRoutes);

module.exports = app;