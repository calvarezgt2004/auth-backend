const User = require("../models/users.model");
const bcrypt = require("bcrypt-nodejs");
const jwt = require("../services/jwt");
let regExp =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

function registerUser(req, res) {
  const params = req.body;
  const modelUser = new User();
  const email = params.email.toLowerCase();
  let emailOk = regExp.test(email);
  if (emailOk != true)
    return res.status(500).send({ message: "Enter a valid email address" });

  if (params.displayname && email && params.password) {
    User.find({ email: email }, (err, userFind) => {
      if (userFind.length === 0) {
        modelUser.displayname = params.displayname;
        modelUser.email = email;
        modelUser.password = params.password;
        bcrypt.hash(params.password, null, null, (err, passwordEncriptada) => {
          modelUser.password = passwordEncriptada;

          modelUser.save((err, usuarioGuardado) => {
            if (err)
              return res.status(500).send({ message: "An error has occurred" });
            if (!usuarioGuardado)
              return res.status(500).send({ message: "Error creating user" });

            return res.status(200).send({ User: usuarioGuardado });
          });
        });
      } else {
        return res.status(400).send({ message: "this email is already taken" });
      }
    });
  } else {
    return res.status(400).send({ message: "Enter the required parameters" });
  }
}

function login(req, res) {
  const params = req.body;
  const email = params.email.toLowerCase();
  let emailOk = regExp.test(email);
  if (emailOk != true)
    return res.status(500).send({ message: "Enter a valid email address" });

  User.findOne({ email: email }, (err, userSearch) => {
    if (err) return res.status(404).send({ message: "An error has occurred" });
    if (!userSearch)
      return res
        .status(500)
        .send({ message: "Email or password is incorrect" });

    //Comparacion de contraseñas

    bcrypt.compare(
      params.password,
      userSearch.password,
      (err, verifyPassword) => {
        //Comparamos si las dos contraseñas
        if (verifyPassword) {
          //si el parametro de la contraseña es correcto, se crea el token si ponemos true en el body,
          //si no pasamos al else

          if (params.obtenerToken === "true") {
            return res.status(200).send({ token: jwt.crearToken(userSearch) });
          } else {
            //Si coinciden, pero no mandan true, se muestra el usuario que se queria logear

            userSearch.password = undefined;
            return res.status(200).send({ user: userSearch });
          }
        } else {
          return res
            .status(500)
            .send({ message: "Email or password is incorrect" });
        }
      }
    );
  });
}

function getUser(req, res) {
  const idUser = req.user.sub;

  User.findById({ _id: idUser }, (err, userFind) => {
    if (err) return res.status(404).send({ message: "An error has occurred" });
    if (!userFind) return res.status(404).send({ message: "User not found" });
    return res.status(200).send({ User: userFind });
  });
}

function editUser(req, res) {
  const idUser = req.user.sub;
  const params = req.body;
  const email = params.email.toLowerCase();
  let emailOk = regExp.test(email);
  if (emailOk != true)
    return res.status(500).send({ message: "Enter a valid email address" });

  User.findByIdAndUpdate(
    idUser,
    { $set: { displayname: params.displayname, email: email } },
    { new: true },
    (err, UserUpdate) => {
      if (err)
        return res.status(500).send({ message: "An error has occurred" });
      if (!UserUpdate)
        return res.status(404).send({ message: "User not found" });
      return res.status(200).send({ userUpdate: UserUpdate });
    }
  );
}

function editPassword(req, res) {
  const idUser = req.user.sub;
  const params = req.body;
  if (!params.newPassword)
    return res.status(403).send({ message: "New password is required" });

  User.findById(idUser, (err, passwordUser) => {
    bcrypt.compare(
      params.oldPassword,
      passwordUser.password,
      (err, verifyPassword) => {
        if (verifyPassword) {
          bcrypt.hash(params.newPassword, null, null, (err, hash) => {
            User.findByIdAndUpdate(
              idUser,
              { password: hash },
              (err, passwordUpdate) => {
                if (err)
                  return res
                    .status(500)
                    .send({ message: "An error has occurred" });
                if (!passwordUpdate)
                  return res
                    .status(500)
                    .send({
                      message:
                        "An error has occurred while updating the password",
                    });
                return res
                  .status(200)
                  .send({ message: "Password updated successfully" });
              }
            );
          });
        } else {
          return res
            .status(500)
            .send({ message: "The password you entered does not match" });
        }
      }
    );
  });
}

function deleteUser(req, res){
  const idUser = req.user.sub

  User.findByIdAndDelete(idUser, (err, userDelete) => {
    if(err) return res.status(500).send({ message: 'An error has occurred'})
    if(!userDelete) return res.status(500).send({ message: "Error delete user" });
    return res.status(200).send({userDelete: userDelete})
  })

}

module.exports = {
  registerUser,
  login,
  getUser,
  editUser,
  editPassword,
  deleteUser
};
