const express = require('express');
const userController = require('../controller/user.controller');

const md_autenticacion = require('../middlewares/autenticacion');

const api = express.Router();

api.post('/register', userController.registerUser)
api.post('/login', userController.login)
api.get('/user', md_autenticacion.Auth, userController.getUser);
api.put('/updateUser', md_autenticacion.Auth, userController.editUser)
api.put('/updatePassword', md_autenticacion.Auth, userController.editPassword)
api.delete('/deleteUser', md_autenticacion.Auth, userController.deleteUser)

module.exports = api;